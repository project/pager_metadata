<?php

/**
 * @file
 * Module implementation file.
 */

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;

/**
 * Implements hook_page_attachments_alter().
 */
function pager_metadata_page_attachments_alter(array &$attachments) {
  $cache = CacheableMetadata::createFromRenderArray($attachments);

  if (Settings::get('pager_metadata_alter_canonical', TRUE)) {
    foreach ($attachments['#attached']['html_head'] as &$attachment) {
      if ($attachment[1] == 'canonical_url') {
        $url = Url::fromUri($attachment[0]['#attributes']['href']);
        $query = $url->getOption('query');
        if ($page = Drupal::service('pager.parameters')->findPage()) {
          if ($page > 0) {
            $query['page'] = $page;
          }
        }
        if (is_array($query)) {
          $url->setOption('query', $query);
        }
        $attachment[0]['#attributes']['href'] = $url->toString();

        // So the URL can be different for each page.
        $cache->addCacheContexts(['url.query_args.pagers']);
      }
    }
  }

  $cache->applyTo($attachments);
}

/**
 * Implements hook_preprocess_HOOK().
 */
function pager_metadata_preprocess_pager(&$pager) {
  _pager_metadata_relations_to_head($pager);
}

/**
 * Implements hook_preprocess_HOOK().
 */
function pager_metadata_preprocess_views_infinite_scroll_pager(&$pager) {
  _pager_metadata_relations_to_head($pager);
}

/**
 * Adds rel attributes to the HTML head.
 *
 * @param array $pager
 *   The pager template variables, as given to pager preprocess functions.
 *
 * @link https://www.drupal.org/project/drupal/issues/1567684#comment-13631375
 */
function _pager_metadata_relations_to_head(array &$pager) {
  $cache = CacheableMetadata::createFromRenderArray($pager);
  $url = Url::fromRoute('<current>', [], ['absolute' => TRUE]);
  $cache->addCacheContexts(['route.name']);
  foreach ([
    'prev' => 'previous',
    'next' => 'next',
  ] as $rel => $item) {
    if (!empty($pager['items'][$item])) {
      $href_url = is_string($pager['items'][$item]['href']) ? Url::fromUserInput($pager['items'][$item]['href']) : $pager['items'][$item]['href'];
      $query = $href_url->getOption('query');
      if (($query['page'] ?? NULL) == 0) {
        unset($query['page']);
      }
      $url->setOption('query', $query);
      $pager['#attached']['html_head_link'][] = [
        [
          'rel' => $rel,
          'href' => $url->toString(),
        ],
        TRUE,
      ];
    }
  }
  $cache->applyTo($pager);
}

/**
 * Implements hook_block_build_alter().
 *
 * @link https://www.drupal.org/project/drupal/issues/2769953#comment-12561253
 */
function pager_metadata_block_build_alter(array &$build, BlockPluginInterface $block) {
  if ($block->getBaseId() == 'views_block') {
    // To make sure the pager preprocess can still insert something the head.
    $build['#create_placeholder'] = FALSE;
  }
}
